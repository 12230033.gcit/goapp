package controller

import (
	"bytes"
	"encoding/json"

	// "io"
	"myapp/model"
	"net/http"
	"net/http/httptest"
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestAddCourse(t *testing.T) {
	course := model.Course{
		CourseID:   1,
		CourseName: "Math",
		// Add other necessary fields
	}
	jsonCourse, _ := json.Marshal(course)
	req, err := http.NewRequest("POST", "/course", bytes.NewBuffer(jsonCourse))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Addcourse)

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusCreated, rr.Code)
	expected := `{"status":"course added"}`
	assert.JSONEq(t, expected, rr.Body.String())
}

func TestGetCourse(t *testing.T) {
	req, err := http.NewRequest("GET", "/course/1", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(GetCourse)

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	expected := `{"CourseID":1,"CourseName":"Math"}`
	assert.JSONEq(t, expected, rr.Body.String())
}

func TestUpdateCourse(t *testing.T) {
	course := model.Course{
		CourseID:   1,
		CourseName: "Updated Math",
		// Add other necessary fields
	}
	jsonCourse, _ := json.Marshal(course)
	req, err := http.NewRequest("PUT", "/course/1", bytes.NewBuffer(jsonCourse))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(UpdateCourse)

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	expected := `{"CourseID":1,"CourseName":"Updated Math"}`
	assert.JSONEq(t, expected, rr.Body.String())
}

func TestDeleteCourse(t *testing.T) {
	req, err := http.NewRequest("DELETE", "/course/1", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(DeleteCourse)

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	expected := `{"status":"deleted"}`
	assert.JSONEq(t, expected, rr.Body.String())
}

func TestGetAllCourse(t *testing.T) {
	req, err := http.NewRequest("GET", "/courses", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(GetAllCourse)

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	// Assert your response body according to your application logic
}
