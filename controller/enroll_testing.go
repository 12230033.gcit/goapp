package controller

// import (
// 	"bytes"
// 	"encoding/json"
// 	"net/http"
// 	"net/http/httptest"
// 	"testing"
// 	"github.com/stretchr/testify/assert"
// 	"myapp/model"      // Import your model package
// )

// func TestEnroll(t *testing.T) {
// 	enrollment := model.Enroll{
// 		StdId:    1,
// 		CourseID: "C001",
// 		// Add other necessary fields
// 	}
// 	jsonEnroll, _ := json.Marshal(enrollment)
// 	req, err := http.NewRequest("POST", "/enroll", bytes.NewBuffer(jsonEnroll))
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	req.Header.Set("Content-Type", "application/json")
// 	rr := httptest.NewRecorder()
// 	handler := http.HandlerFunc(controller.Enroll)

// 	handler.ServeHTTP(rr, req)

// 	assert.Equal(t, http.StatusCreated, rr.Code)
// 	expected := `{"status":"enrolled"}`
// 	assert.JSONEq(t, expected, rr.Body.String())
// }

// func TestGetEnroll(t *testing.T) {
// 	req, err := http.NewRequest("GET", "/enroll/1/C001", nil)
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	rr := httptest.NewRecorder()
// 	handler := http.HandlerFunc(controller.GetEnroll)

// 	handler.ServeHTTP(rr, req)

// 	assert.Equal(t, http.StatusOK, rr.Code)
// 	// Assert your response body according to your application logic
// }

// func TestGetAllEnrolls(t *testing.T) {
// 	req, err := http.NewRequest("GET", "/enrolls", nil)
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	rr := httptest.NewRecorder()
// 	handler := http.HandlerFunc(controller.GetEnrolls)

// 	handler.ServeHTTP(rr, req)

// 	assert.Equal(t, http.StatusOK, rr.Code)
// 	// Assert your response body according to your application logic
// }

// func TestDeleteEnroll(t *testing.T) {
// 	req, err := http.NewRequest("DELETE", "/enroll/1/C001", nil)
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	rr := httptest.NewRecorder()
// 	handler := http.HandlerFunc(controller.DeleteEnroll)

// 	handler.ServeHTTP(rr, req)

// 	assert.Equal(t, http.StatusOK, rr.Code)
// 	expected := `{"status":"deleted"}`
// 	assert.JSONEq(t, expected, rr.Body.String())
// }
